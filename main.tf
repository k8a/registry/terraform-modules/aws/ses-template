module "s3_bucket" {
  count = var.action_type == "s3" ? 1 : 0

  source = "./modules/s3_bucket"

  bucket_name = var.s3_action.bucket_name
}

module "sns_topic" {
  count = var.action_type == "sns" ? 1 : 0

  source = "./modules/sns_topic"

  providers = {
    aws          = aws
    aws.virginia = aws.virginia
  }

  topic_name = var.sns_action.topic_name
  protocol   = var.sns_action.protocol
  endpoint   = var.sns_action.endpoint
}

module "ses_receipt" {
  source = "./modules/ses_receipt"

  providers = {
    aws          = aws
    aws.virginia = aws.virginia
  }

  domain_name    = var.domain_name
  hosted_zone_id = var.hosted_zone_id
  action_type    = var.action_type
  bucket_name    = var.action_type == "s3" ? module.s3_bucket.0.bucket_name : null
  topic_arn      = var.action_type == "sns" ? module.sns_topic.0.topic_arn : null
}
