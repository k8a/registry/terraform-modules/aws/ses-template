# AWS SES Template

## Usage

### with S3

```tf
module "ses" {
  source = "git@gitlab.com:k8a/registry/terraform-modules/aws/ses-template.git?ref=dev"

  providers = {
    aws          = aws
    aws.virginia = aws.virginia
  }

  domain_name    = "mail.example.com"
  hosted_zone_id = "xxx"

  action_type = "s3"
  sns_action = {
    bucket_name = "example-mail-bucket"
  }
}
```

### with SNS

```tf
module "ses" {
  source = "git@gitlab.com:k8a/registry/terraform-modules/aws-ses-template.git?ref=dev"

  providers = {
    aws          = aws
    aws.virginia = aws.virginia
  }

  domain_name    = "mail.example.com"
  hosted_zone_id = "xxx"

  action_type = "sns"
  sns_action = {
    endpoint   = "hoge@gmail.com"
    protocol   = "email"
    topic_name = "ses-to-gmail"
  }
}
```
