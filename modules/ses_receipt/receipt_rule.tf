resource "aws_ses_receipt_rule_set" "rule_set" {
  provider = aws.virginia

  rule_set_name = var.domain_name
}

resource "aws_ses_active_receipt_rule_set" "active_rule_set" {
  provider = aws.virginia

  rule_set_name = aws_ses_receipt_rule_set.rule_set.rule_set_name
}

resource "aws_ses_receipt_rule" "rule" {
  provider = aws.virginia

  name          = var.domain_name
  rule_set_name = aws_ses_receipt_rule_set.rule_set.rule_set_name
  recipients = [
    var.domain_name
  ]
  enabled      = true
  scan_enabled = true

  dynamic "s3_action" {
    for_each = var.action_type == "s3" ? ["dummy"] : []
    content {
      bucket_name = var.bucket_name
      position    = 1
    }
  }

  dynamic "sns_action" {
    for_each = var.action_type == "sns" ? ["dummy"] : []
    content {
      topic_arn = var.topic_arn
      position  = 1
    }
  }
}
