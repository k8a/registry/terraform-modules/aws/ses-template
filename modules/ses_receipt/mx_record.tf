resource "aws_route53_record" "mx_record" {
  zone_id = var.hosted_zone_id
  name    = var.domain_name
  type    = "MX"
  ttl     = "60"
  records = ["10 inbound-smtp.us-east-1.amazonaws.com"]
}
