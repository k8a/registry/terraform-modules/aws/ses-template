variable "domain_name" {
  type = string
}

variable "hosted_zone_id" {
  type = string
}

variable "action_type" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "topic_arn" {
  type = string
}
