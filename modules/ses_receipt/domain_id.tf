resource "aws_ses_domain_identity" "domain_id" {
  provider = aws.virginia

  domain = var.domain_name
}

resource "aws_route53_record" "ses_validation" {
  zone_id = var.hosted_zone_id
  name    = "_amazonses.${var.domain_name}"
  type    = "TXT"
  ttl     = "60"
  records = [
    aws_ses_domain_identity.domain_id.verification_token
  ]
}
