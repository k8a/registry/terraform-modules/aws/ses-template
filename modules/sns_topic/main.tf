resource "aws_sns_topic" "topic" {
  provider = aws.virginia

  name = var.topic_name
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "topic_policy_doc" {
  statement {
    principals {
      type        = "Service"
      identifiers = ["ses.amazonaws.com"]
    }

    actions = [
      "SNS:GetTopicAttributes",
      "SNS:SetTopicAttributes",
      "SNS:AddPermission",
      "SNS:RemovePermission",
      "SNS:DeleteTopic",
      "SNS:Subscribe",
      "SNS:ListSubscriptionsByTopic",
      "SNS:Publish"
    ]

    resources = [
      aws_sns_topic.topic.arn,
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.current.account_id,
      ]
    }
  }
}

resource "aws_sns_topic_policy" "topic_policy" {
  provider = aws.virginia

  arn    = aws_sns_topic.topic.arn
  policy = data.aws_iam_policy_document.topic_policy_doc.json
}

resource "aws_sns_topic_subscription" "topic_subscription" {
  provider = aws.virginia

  topic_arn = aws_sns_topic.topic.arn
  protocol  = var.protocol
  endpoint  = var.endpoint
}
