variable "topic_name" {
  type = string
}

variable "protocol" {
  type = string
}

variable "endpoint" {
  type = string
}
