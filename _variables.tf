variable "domain_name" {
  type = string
}

variable "hosted_zone_id" {
  type = string
}

variable "action_type" {
  type = string
}

variable "s3_action" {
  type = object({
    bucket_name = string
  })
  default = {
    bucket_name = null
  }
}

variable "sns_action" {
  type = object({
    topic_name = string
    protocol   = string
    endpoint   = string
  })
  default = {
    topic_name = null
    protocol   = null
    endpoint   = null
  }
}
